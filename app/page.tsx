import Image from "next/image";
import styles from "./page.module.css";
import Table from "@/app/Table";

export default function Home() {
  return (
    <main className={styles.main}>
      <Table/>
    </main>
  );
}
