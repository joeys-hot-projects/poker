"use client";
import { FC, useEffect, useState } from "react";
import styles from "./page.module.css";
import useGameStore from "@/app/gameState";
import PokerCard from "@/app/PokerCard";

const Table: FC = () => {
  const {
    players,
    nextHand,
    currentPlayer,
    fold,
    checkOrCall,
    betOrRaise,
    minBet,
    biggestActiveBet,
    potSize,
    tableCards,
  } = useGameStore();
  const [betSize, setBetSize] = useState(minBet);
  const [gameStarted, setGameStarted] = useState(false);
  return (
    <>
      {!true ? (
        <div>
          <button
            onClick={() => {
              setGameStarted(true);
              nextHand();
            }}
          >
            Play Poker
          </button>
        </div>
      ) : (
        <>
          <div className={styles.table}>
            {Object.values(players).map((player, index) => (
              <div key={index} style={{ height: "50%" }}>
                <h4 style={{ fontWeight: index === currentPlayer ? 800 : 400 }}>
                  {player.name}
                </h4>
                {player.hand && player.isIn && (
                  <div style={{ display: "flex", gap: "12px" }}>
                    <PokerCard {...player.hand[0]} />
                    <PokerCard {...player.hand[1]} />
                  </div>
                )}
                <p>Stack {player.chips} $</p>
                <p>Bet: {player.betSize} $</p>
              </div>
            ))}
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                gridArea: "2 / 2 / 3 / 4",
                gap: "4px"
              }}
            >
              {Array.from(Array(3)).map((_, index) => (
                <PokerCard key={index} {...(tableCards.flop ? tableCards.flop[index] : {})} />
              ))}
              <PokerCard {...tableCards.turn} />
              <PokerCard {...tableCards.river} />
            </div>
          </div>
          <p>Current Pot: {potSize}</p>
          <input
            type={"number"}
            max={players[currentPlayer].chips}
            min={biggestActiveBet + minBet}
            step={minBet}
            value={betSize}
            onChange={(event) => setBetSize(Number(event.target.value))}
          />
          ;
          <div className={styles.actionButtons}>
            <button onClick={() => fold()}>Fold</button>
            <button onClick={() => checkOrCall()}>Check/Call</button>
            <button onClick={() => betOrRaise(betSize)}>Bet/Raise</button>
          </div>
          ;
        </>
      )}
    </>
  );
};

export default Table;
