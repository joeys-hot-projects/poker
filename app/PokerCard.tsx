import { FC } from "react";
import { CardColor, CardRank, PlayingCard } from "@/app/types";
import styles from "./page.module.css";
import Diamonds from "@/public/diamonds.png";
import Spades from "@/public/Spades.png";
import Hearts from "@/public/Hearts.png";
import Clubs from "@/public/clubs.png";
import Image, { StaticImageData } from "next/image";

const imageMap: Record<CardColor, StaticImageData> = {
  diamonds: Diamonds,
  hearts: Hearts,
  spades: Spades,
  clubs: Clubs,
};
const PokerCard: FC<{ color?: CardColor; rank?: CardRank }> = ({
  color,
  rank,
}) => {
  if (!color || !rank) return <div className={styles.playingCardPlaceholder}></div>;
  return (
    <div className={styles.playingCard}>
      <p>{rank}</p>
      <p></p>
      <p>{rank}</p>
      <p></p>
      <Image src={imageMap[color]} alt={color} height={20} width={16} />
      <p></p>
      <p>{rank}</p>
      <p></p>
      <p>{rank}</p>
    </div>
  );
};

export default PokerCard;
