import { CardDeck } from "@/app/types";

export const INITIAL_CHIPS = 1500;
export const BLIND_PROGRESSION = [10, 20, 40, 50, 100, 200, 400, 500, 1000, 2000, 5000];
export const HANDS_PLAYED_PER_BLIND = 12;
export const COMPLETE_DECK: CardDeck = [
  {
    color: "diamonds",
    rank: "2"
  },
  {
    color: "diamonds",
    rank: "3"
  },
  {
    color: "diamonds",
    rank: "4"
  },
  {
    color: "diamonds",
    rank: "5"
  },
  {
    color: "diamonds",
    rank: "6"
  },
  {
    color: "diamonds",
    rank: "7"
  },
  {
    color: "diamonds",
    rank: "8"
  },
  {
    color: "diamonds",
    rank: "9"
  },
  {
    color: "diamonds",
    rank: "10"
  },
  {
    color: "diamonds",
    rank: "J"
  },
  {
    color: "diamonds",
    rank: "Q"
  },
  {
    color: "diamonds",
    rank: "K"
  },
  {
    color: "diamonds",
    rank: "A"
  },
  {
    color: "spades",
    rank: "2"
  },
  {
    color: "spades",
    rank: "3"
  },
  {
    color: "spades",
    rank: "4"
  },
  {
    color: "spades",
    rank: "5"
  },
  {
    color: "spades",
    rank: "6"
  },
  {
    color: "spades",
    rank: "7"
  },
  {
    color: "spades",
    rank: "8"
  },
  {
    color: "spades",
    rank: "9"
  },
  {
    color: "spades",
    rank: "10"
  },
  {
    color: "spades",
    rank: "J"
  },
  {
    color: "spades",
    rank: "Q"
  },
  {
    color: "spades",
    rank: "K"
  },
  {
    color: "spades",
    rank: "A"
  },
  {
    color: "clubs",
    rank: "2"
  },
  {
    color: "clubs",
    rank: "3"
  },
  {
    color: "clubs",
    rank: "4"
  },
  {
    color: "clubs",
    rank: "5"
  },
  {
    color: "clubs",
    rank: "6"
  },
  {
    color: "clubs",
    rank: "7"
  },
  {
    color: "clubs",
    rank: "8"
  },
  {
    color: "clubs",
    rank: "9"
  },
  {
    color: "clubs",
    rank: "10"
  },
  {
    color: "clubs",
    rank: "J"
  },
  {
    color: "clubs",
    rank: "Q"
  },
  {
    color: "clubs",
    rank: "K"
  },
  {
    color: "clubs",
    rank: "A"
  },
  {
    color: "hearts",
    rank: "2"
  },
  {
    color: "hearts",
    rank: "3"
  },
  {
    color: "hearts",
    rank: "4"
  },
  {
    color: "hearts",
    rank: "5"
  },
  {
    color: "hearts",
    rank: "6"
  },
  {
    color: "hearts",
    rank: "7"
  },
  {
    color: "hearts",
    rank: "8"
  },
  {
    color: "hearts",
    rank: "9"
  },
  {
    color: "hearts",
    rank: "10"
  },
  {
    color: "hearts",
    rank: "J"
  },
  {
    color: "hearts",
    rank: "Q"
  },
  {
    color: "hearts",
    rank: "K"
  },
  {
    color: "hearts",
    rank: "A"
  },
]