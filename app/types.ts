export type CardColor = "hearts" | "clubs" | "diamonds" | "spades";
export type CardRank =
  | "2"
  | "3"
  | "4"
  | "5"
  | "6"
  | "7"
  | "8"
  | "9"
  | "10"
  | "J"
  | "Q"
  | "K"
  | "A";

export interface PlayingCard {
  color: CardColor;
  rank: CardRank;
}

export type CardDeck = Array<PlayingCard>;

export type PlayerHand = [PlayingCard, PlayingCard];
export type Flop = [PlayingCard, PlayingCard, PlayingCard];
export type Turn = PlayingCard;
export type River = PlayingCard;
export type TableCards = Partial<{
  flop: Flop;
  turn: Turn;
  river: River;
}>;

export interface Player {
  name: string;
  hand: PlayerHand | null;
  chips: number;
  isIn: boolean;
  betSize: number;
}

export interface CommonInfo {
  smallBlind: number;
  turnsUntilBlindIncreases: number;
  dealerIndex: number;
  players: Record<string, Player>;
  tableCards: TableCards;
  potSize: number;
  deck: CardDeck;
  currentPlayer: number;
  minBet: number;
  biggestActiveBet: number;
}
