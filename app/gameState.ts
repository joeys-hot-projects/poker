import { CommonInfo, Player } from "@/app/types";
import { create } from "zustand";
import {
  BLIND_PROGRESSION,
  COMPLETE_DECK,
  HANDS_PLAYED_PER_BLIND,
  INITIAL_CHIPS,
} from "@/app/constants";
import table from "@/app/Table";
import { persist } from "zustand/middleware";

type GameState = CommonInfo;

interface GameStore extends GameState {
  handOutCards: () => void;
  fold: () => void;
  checkOrCall: () => void;
  betOrRaise: (amount: number) => void;
  nextPlayer: () => void;
  nextPhase: () => void;
  resolveHands: () => void;
  nextHand: () => void;
}

const useGameStore = create<GameStore>()(
  persist(
    (setState, getState) => ({
      deck: COMPLETE_DECK,
      smallBlind: 0,
      dealerIndex: 0,
      tableCards: {},
      turnsUntilBlindIncreases: HANDS_PLAYED_PER_BLIND,
      potSize: 0,
      currentPlayer: 1,
      minBet: BLIND_PROGRESSION[0] * 2,
      biggestActiveBet: 0,
      players: {
        0: {
          name: "Joey",
          isIn: true,
          betSize: 0,
          chips: INITIAL_CHIPS,
          hand: null,
        },
        1: {
          name: "Sam",
          isIn: true,
          betSize: 0,
          chips: INITIAL_CHIPS,
          hand: null,
        },
        2: {
          name: "Dean",
          isIn: true,
          betSize: 0,
          chips: INITIAL_CHIPS,
          hand: null,
        },
        3: {
          name: "Tom",
          isIn: true,
          betSize: 0,
          chips: INITIAL_CHIPS,
          hand: null,
        },
      },
      handOutCards: () =>
        setState((state) => {
          const { players } = { ...state };
          const shuffledCards = [...COMPLETE_DECK];
          for (let i = shuffledCards.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            const temp = shuffledCards[i];
            shuffledCards[i] = shuffledCards[j];
            shuffledCards[j] = temp;
          }
          Object.entries(players).forEach(([index, player]) => {
            const firstCard = shuffledCards.pop();
            const secondCard = shuffledCards.pop();
            players[index] = {
              ...player,
              hand: firstCard && secondCard ? [firstCard, secondCard] : null,
              isIn: true,
            };
          });
          return {
            players,
            deck: shuffledCards,
          };
        }),
      nextHand: () => {
        console.log("next hand");
        const state = { ...getState() };
        const newDealer =
          state.dealerIndex === Object.entries(state.players).length
            ? 0
            : state.dealerIndex + 1;

        const smallBlindIncrease = state.turnsUntilBlindIncreases === 0;
        const smallBlind = smallBlindIncrease
          ? state.smallBlind + 1
          : state.smallBlind;
        const smallBlindPlayer = (newDealer + 1) % 4;
        const bigBlindPlayer = (smallBlindPlayer + 1) % 4;
        const currentPlayer =
          bigBlindPlayer + 1 === Object.entries(state.players).length
            ? 0
            : bigBlindPlayer + 1;
        const players = { ...state.players };
        players[smallBlindPlayer].chips -= BLIND_PROGRESSION[smallBlind];
        players[smallBlindPlayer].betSize = BLIND_PROGRESSION[smallBlind];
        players[bigBlindPlayer].chips -= 2 * BLIND_PROGRESSION[smallBlind];
        players[bigBlindPlayer].betSize = 2 * BLIND_PROGRESSION[smallBlind];

        setState((state) => ({
          dealerIndex: newDealer,
          turnsUntilBlindIncreases: smallBlindIncrease
            ? HANDS_PLAYED_PER_BLIND
            : state.turnsUntilBlindIncreases - 1,
          smallBlind,
          currentPlayer,
          players,
          minBet: BLIND_PROGRESSION[smallBlind] * 2,
          biggestActiveBet: BLIND_PROGRESSION[smallBlind] * 2,
          tableCards: {
            turn: undefined,
            flop: undefined,
            river: undefined,
          },
        }));
        state.handOutCards();
        console.log(state.players);
      },
      resolveHands: () => {
        const state = { ...getState() };
        state.nextHand();
      },
      nextPhase: () => {
        console.log("nextphase");
        const state = { ...getState() };
        Object.values(state.players).forEach((player) => {
          state.potSize += player.betSize;
          player.betSize = 0;
        });
        setState({
          players: state.players,
          potSize: state.potSize,
          biggestActiveBet: 0,
        });
        const playersStillIn = Object.entries(state.players).filter(
          ([_, player]) => player.isIn,
        );
        if (playersStillIn.length === 1) {
          const winner = playersStillIn[0];
          state.players[winner[0]].chips += state.potSize;
          state.potSize = 0;
          setState(state);
          return state.nextHand();
        } else {
          if (state.tableCards.river) {
            state.resolveHands();
            return;
          } else {
            let nextPlayer = state.dealerIndex;
            Object.values(state.players).some(() => {
              // return the next player that is in and
              if (nextPlayer < Object.values(state.players).length - 1) {
                nextPlayer++;
              } else {
                nextPlayer = 0;
              }
              if (state.players[nextPlayer].isIn) {
                return true;
              }
            });
            if (state.tableCards.turn) {
              console.log("huh");
              state.tableCards.river = state.deck.pop();
            } else if (state.tableCards.flop) {
              console.log("hah");
              state.tableCards.turn = state.deck.pop();
            } else {
              const firstFlopCard = state.deck.pop();
              const secondFlopCard = state.deck.pop();
              const thirdFlopCard = state.deck.pop();
              state.tableCards.flop = firstFlopCard &&
                secondFlopCard &&
                thirdFlopCard && [firstFlopCard, secondFlopCard, thirdFlopCard];
            }
            setState({
              currentPlayer: nextPlayer,
              tableCards: state.tableCards,
              deck: state.deck,
            });
          }
        }
      },
      nextPlayer: () => {
        const state = { ...getState() };
        let nextPlayer = state.currentPlayer;
        const playersStillIn = Object.entries(state.players).filter(
          ([_, player]) => player.isIn,
        );
        if (playersStillIn.length === 1) {
          // if only one player remains, hand ends and there is no next player.
          return state.nextPhase();
        }
        let biggestBetSize = state.players[nextPlayer].betSize;
        Object.values(state.players).forEach((player) => {
          // get the biggest bet
          if (player.betSize > biggestBetSize) biggestBetSize = player.betSize;
        });
        if (biggestBetSize === 0) {
          // If everyone checked or folded
          if (nextPlayer === state.dealerIndex) {
            //and current player is dealer, next phase.
            return state.nextPhase();
          }

          Object.values(state.players).some(() => {
            // return the next player that is in and
            if (nextPlayer < Object.values(state.players).length - 1) {
              nextPlayer++;
            } else {
              nextPlayer = 0;
            }
            if (state.players[nextPlayer].isIn) {
              return true;
            }
          });

          return setState({
            currentPlayer: nextPlayer,
          });
        }

        Object.values(state.players).some(() => {
          // return the next player that is in and
          if (nextPlayer < Object.values(state.players).length - 1) {
            nextPlayer++;
          } else {
            nextPlayer = 0;
          }
          if (
            state.players[nextPlayer].isIn &&
            state.players[nextPlayer].betSize < biggestBetSize
          ) {
            return true;
          }
        });
        if (nextPlayer === state.currentPlayer) {
          return state.nextPhase();
        }
        setState({
          currentPlayer: nextPlayer,
        });
      },
      betOrRaise: (amount) => {
        const state = getState();
        const currentPlayer = state.players[state.currentPlayer];
        const previousBetSize = currentPlayer.betSize;
        let biggestBetSize = previousBetSize;
        Object.entries(state.players).forEach(([index, player]) => {
          // get biggest bet size
          if (player.betSize > biggestBetSize) biggestBetSize = player.betSize;
        });
        const additionalBetSize = amount - previousBetSize;
        const reRaiseSize = amount - biggestBetSize;
        if (reRaiseSize < state.minBet) {
          console.warn("Bet too smol");
          return;
        }
        if (additionalBetSize > currentPlayer.chips) {
          console.warn("Bet too big");
          return;
        }
        state.players[state.currentPlayer].betSize += additionalBetSize;
        state.players[state.currentPlayer].chips -= additionalBetSize;
        setState({
          players: state.players,
          minBet: reRaiseSize,
          biggestActiveBet: amount,
        });
        return state.nextPlayer();
      },
      checkOrCall: () => {
        const state = getState();
        const players = { ...state.players };
        const currentPlayer = players[state.currentPlayer];
        let biggestBetSize = currentPlayer.betSize;
        Object.entries(state.players).forEach(([index, player]) => {
          // get biggest bet size
          if (player.betSize > biggestBetSize) biggestBetSize = player.betSize;
        });
        if (biggestBetSize === currentPlayer.betSize) {
          // if biggest bet is equal to currentPlayers bet, they checked and nothing needs to happen
          return state.nextPlayer();
        } else {
          // if they called they bet the difference between their bet and the highest bet
          currentPlayer.chips -= biggestBetSize - currentPlayer.betSize;
          currentPlayer.betSize = biggestBetSize;
          return state.nextPlayer();
        }
      },
      fold: () => {
        const state = getState();
        const players = { ...state.players };
        const currentPlayer = players[state.currentPlayer];
        state.potSize += currentPlayer.betSize;
        currentPlayer.betSize = 0;
        currentPlayer.isIn = false;
        players[state.currentPlayer] = currentPlayer;
        setState(() => ({
          ...state,
          players,
        }));
        state.nextPlayer();
      },
    }),
    {
      name: "poker cache",
      partialize: (state) => ({
        players: Object.fromEntries(
          Object.entries(state.players).map(
            ([index, player]): [string, Player] => {
              return [
                index,
                {
                  ...player,
                  chips:
                    player.chips +
                    player.betSize +
                    Math.round(state.potSize / 4),
                  betSize: 0,
                },
              ];
            },
          ),
        ),
        smallBlind: state.smallBlind,
        turnsUntilBlindIncreases: state.turnsUntilBlindIncreases,
        dealerIndex: state.dealerIndex,
      }),
    },
  ),
);

export default useGameStore;
